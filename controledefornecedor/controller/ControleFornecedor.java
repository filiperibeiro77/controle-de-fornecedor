package controledefornecedor.controller;

import controledefornecedor.modell.Fornecedor;
import controledefornecedor.modell.PessoaFisica;
import controledefornecedor.modell.PessoaJuridica;
import java.util.ArrayList;

public class ControleFornecedor {
    private ArrayList<Fornecedor> listaFornecedor;
    
    public ControleFornecedor(){
        listaFornecedor = new ArrayList<Fornecedor>();
    }
    
    public String adicionarPessoaFisica(PessoaFisica umaPessoaFisica){
        listaFornecedor.add(umaPessoaFisica);
        String mensagem = "Pessoa Física "+umaPessoaFisica.getNome()+"adicionada como fornecedora";
        return mensagem;
    }
    
    public String adicionarPessoaJuridica(PessoaJuridica umaPessoaJuridica){
        listaFornecedor.add(umaPessoaJuridica);
        String mensagem = "Pessoa Jurídica"+umaPessoaJuridica.getNome()+" adicionada como fornecedora";
        return mensagem;
    }
    
    public String excluirPessoaFisica(PessoaFisica umaPessoaFisica){
        listaFornecedor.remove(umaPessoaFisica);
        String mensagem = ""+umaPessoaFisica.getNome()+" foi excluido com sucesso!";
        return mensagem;
    }
    
     public String excluirPessoaJuridica(PessoaJuridica umaPessoaJuridica){
        listaFornecedor.remove(umaPessoaJuridica);
        String mensagem = ""+umaPessoaJuridica.getNome()+" foi excluido com sucesso!";
        return mensagem;
    }
    
    public Fornecedor pesquisarFornecedor(String umNome){
        for(Fornecedor umFornecedor: listaFornecedor){
            if(umFornecedor.getNome().equalsIgnoreCase(umNome)){
                return umFornecedor;
            }
        }
        return null;
    }
    
}