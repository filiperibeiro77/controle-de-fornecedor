package controledefornecedor.modell;

public class PessoaFisica extends Fornecedor{
    private String cpf;
	
	 public PessoaFisica(String nome, String telefone, String cpf){
		  super(nome, telefone);
		  this.cpf = cpf;
	 }
	
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
